﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitClass : MonoBehaviour 
{
	public void EXIT()
	{
		UnityEngine.Debug.Log("goodbuy");
		//Выход
		//if (!Application.isEditor) System.Diagnostics.Process.GetCurrentProcess().Kill();
		//Application.Quit();
		
		#if (UNITY_EDITOR)
			UnityEditor.EditorApplication.isPlaying = false;
		#elif (UNITY_STANDALONE) 
			Application.Quit();
		#elif (UNITY_WEBGL)
			Application.OpenURL("about:blank");
		#endif
	}
}
	

